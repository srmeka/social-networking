﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meta
{
    internal class Signup
    {
        private string name;
        private string email;
        private string phonenumber;
        private string gender;
        private string password;

        public Signup(string name, string email, string phonenumber, string gender, string password)
        {
            this.name = name;
            this.email = email;
            this.phonenumber = phonenumber;
            this.gender = gender;
            this.password = password;
        }

        //here converting Signup object to string representation
        override
            public string ToString()
        {
            String stringToReturn = "This is a Signup object details: \n";
            stringToReturn += "Name:         " + this.name + "\n";
            stringToReturn += "E-mail:       " + this.email + "\n";
            stringToReturn += "Phone Number: " + this.phonenumber + "\n";
            stringToReturn += "gender:       " + this.gender + "\n";
            stringToReturn += "password:     " + this.password + "\n";
            return stringToReturn;



        }

    }
}





        
                                                                                                                                